﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class UserReceipt: BaseEntity
	{
		public Guid? UserId { get; set; }
		public Guid? ReceiptId { get; set; }


		public User User { get; set; }
		public Receipt Receipt { get; set; }
	}
}
