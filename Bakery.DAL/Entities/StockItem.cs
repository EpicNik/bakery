﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class StockItem:BaseEntity
	{
		public Guid? ProductId { get; set; }
		public Guid? StockId { get; set; }


		public Product Product { get; set; }
		public Stock Stock { get; set; }
	}
}
