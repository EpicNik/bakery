﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class ReceiptProduct:BaseEntity
	{
		public Guid? ReceiptId { get; set; }
		public Guid? ProductId { get; set; }


		public Receipt Receipt { get; set; }
		public Product Product { get; set; }
	}
}
