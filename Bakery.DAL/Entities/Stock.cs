﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class Stock:BaseEntity
	{
		public string NameStock { get; set; }
		public DateTime StartStock { get; set; }
		public DateTime EndStock { get; set; }
		public double sale { get; set; }


		public IEnumerable<StockItem> StockItems { get; set; }
	}
}
