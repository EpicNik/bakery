﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class Log : BaseEntity
	{
		public string Data { get; set; }
		public string Discription { get; set; }
	}
}
