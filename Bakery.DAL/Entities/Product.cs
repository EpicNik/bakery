﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class Product:BaseEntity
	{
		public string NameProduct { get; set; }
		public double Price { get; set; }
		public string Description { get; set; }


		public Guid? ProductCategoryId { get; set; }


		public ProductCategory ProductCategory { get; set; }


		public IEnumerable<BasketProduct> BasketProducts { get; set; }
		public IEnumerable<StockItem> StockItems { get; set; }
		public IEnumerable<ReceiptProduct> ReceiptProducts { get; set; }
	}
}
