﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class ProductCategory:BaseEntity 
	{
		public string NameProductCategory { get; set; }


		public IEnumerable<Product> Products { get; set; }
	}
}
