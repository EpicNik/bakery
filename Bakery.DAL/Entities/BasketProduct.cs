﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class BasketProduct:BaseEntity 
	{
		public Guid? BasketId { get; set; }
		public Guid? ProductId { get; set; }

		public Basket Basket { get; set; }
		public Product Product { get; set; }
	}
}
