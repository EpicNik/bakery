﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class Basket:BaseEntity
	{
		public int Count { get; set; }


		public Guid? UserId { get; set; }


		public User User { get; set; }


		public IEnumerable<BasketProduct> BasketProducts { get; set; }
	}
}
