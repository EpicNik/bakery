﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class Receipt:BaseEntity
	{
		public DateTime Date { get; set; }

		public Guid? UserId { get; set; }


		public User User { get; set; }


		public IEnumerable<ReceiptProduct> ReceiptProducts { get; set; }
		public IEnumerable<UserReceipt> UserReceipts { get; set; }
	}
}
