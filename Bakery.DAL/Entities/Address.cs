﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class Address: BaseEntity
	{
		public string City { get; set; }
		public string Street { get; set; }
		public int HouseNumber { get; set; }
		public int Flat { get; set; }
		public int Floot { get; set; }
		public int Entrance { get; set; }
		public int DoorCode { get; set; }


		public IEnumerable<User> Users { get; set; }
	}
}
