﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Entities
{
	public class User : BaseEntity
	{
		public string Email { get; set; }
		public string Password { get; set; }
		public string Name { get; set; }
		public string Phone { get; set; }


		public Guid? RoleId { get; set; }
		public Guid? AddressId { get; set; }


		public Role Role { get; set; }
		public Address Address { get; set; }


		public IEnumerable<Review> Reviews { get; set; }
		public IEnumerable<Basket> Baskets { get; set; }
		public IEnumerable<Receipt> Receipts { get; set; }
		public IEnumerable<UserReceipt> UserReceipts { get; set; }
	}
}
