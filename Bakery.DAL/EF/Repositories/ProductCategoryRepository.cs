﻿using Bakery.DAL.Entities;
using Bakery.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.EF.Repositories
{
	public class ProductCategoryRepository:GenericRepository<ProductCategory>, IProductCategoryRepository
	{
		public ProductCategoryRepository(BakeryDBContext context) : base(context)
		{

		}
	}
}
