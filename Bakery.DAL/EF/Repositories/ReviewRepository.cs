﻿using Bakery.DAL.Entities;
using Bakery.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.EF.Repositories
{
	public class ReviewRepository: GenericRepository<Review>, IReviewRepository
	{
		public ReviewRepository(BakeryDBContext context) : base(context)
		{

		}
	}
}
