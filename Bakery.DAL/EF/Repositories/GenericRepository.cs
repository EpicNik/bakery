﻿using Bakery.DAL.Interfaces;
using Bakery.DAL.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.EF.Repositories
{
	public abstract class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, IBaseEntity
	{
		private readonly BakeryDBContext _context;

		public GenericRepository(BakeryDBContext context)
		{
			_context = context;
		}
		public virtual IQueryable<TEntity> GetAll()
		{
			return _context.Set<TEntity>();
		}

		public virtual async Task<TEntity> GetById(Guid id)
		{
			return await _context.Set<TEntity>().FirstOrDefaultAsync(entity => entity.Id == id);
		}

		public virtual IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> expression)
		{
			return _context.Set<TEntity>().Where(expression);
		}

		public async Task<TEntity> Add(TEntity entity)
		{
			_context.Set<TEntity>().Add(entity);
			await _context.SaveChangesAsync();
			return entity;
		}

		public async Task<TEntity> Update(TEntity entity)
		{
			_context.Entry(entity).State = EntityState.Modified;
			await _context.SaveChangesAsync();
			return entity;
		}

		public async Task<TEntity> Delete(Guid id)
		{
			var entity = await _context.Set<TEntity>().FindAsync(id);
			if (entity == null)
			{
				return entity;
			}

			_context.Set<TEntity>().Remove(entity);
			await _context.SaveChangesAsync();
			return entity;
		}
	}
}
