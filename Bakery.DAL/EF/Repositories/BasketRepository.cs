﻿using Bakery.DAL.Entities;
using Bakery.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.EF.Repositories
{
	public class BasketRepository : GenericRepository<Basket>, IBasketRepository
	{
		public BasketRepository(BakeryDBContext context) : base(context)
		{

		}
	}
}
