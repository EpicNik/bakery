using Bakery.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.EF
{
	public class BakeryDBContext : DbContext
	{
		public BakeryDBContext(DbContextOptions<BakeryDBContext> options) : base(options)
		{
		}

		public DbSet<Address> Addresses { get; set; }
		public DbSet<Basket> Baskets { get; set; }
		public DbSet<BasketProduct> BasketProducts { get; set; }
		public DbSet<Product> Products { get; set; }
		public DbSet<ProductCategory> ProductCategories { get; set; }
		public DbSet<Receipt> Receipts { get; set; }
		public DbSet<ReceiptProduct> ReceiptProducts { get; set; }
		public DbSet<Review> Reviews { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<Stock> Stocks { get; set; }
		public DbSet<StockItem> StockItems { get; set; }
		public DbSet<User> Users { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>()
				.HasOne<Role>(s => s.Role)
				.WithMany(g => g.Users)
				.HasForeignKey(s => s.RoleId);
			modelBuilder.Entity<User>()
				.HasOne<Address>(s => s.Address)
				.WithMany(g => g.Users)
				.HasForeignKey(s => s.AddressId);
			modelBuilder.Entity<Review>()
				.HasOne<User>(s => s.User)
				.WithMany(g => g.Reviews)
				.HasForeignKey(s => s.UserId);
			modelBuilder.Entity<Basket>()
				.HasOne<User>(s => s.User)
				.WithMany(g => g.Baskets)
				.HasForeignKey(s => s.UserId);
			modelBuilder.Entity<BasketProduct>()
				.HasOne<Basket>(s => s.Basket)
				.WithMany(g => g.BasketProducts)
				.HasForeignKey(s => s.BasketId);
			modelBuilder.Entity<Receipt>()
				.HasOne<User>(s => s.User)
				.WithMany(g => g.Receipts)
				.HasForeignKey(s => s.UserId);
			modelBuilder.Entity<StockItem>()
				.HasOne<Product>(s => s.Product)
				.WithMany(g => g.StockItems)
				.HasForeignKey(s => s.ProductId);
			modelBuilder.Entity<StockItem>()
				.HasOne<Stock>(s => s.Stock)
				.WithMany(g => g.StockItems)
				.HasForeignKey(s => s.StockId);
			modelBuilder.Entity<ReceiptProduct>()
				.HasOne<Product>(s => s.Product)
				.WithMany(g => g.ReceiptProducts)
				.HasForeignKey(s => s.ProductId);
			modelBuilder.Entity<ReceiptProduct>()
				.HasOne<Receipt>(s => s.Receipt)
				.WithMany(g => g.ReceiptProducts)
				.HasForeignKey(s => s.ReceiptId);
			modelBuilder.Entity<Product>()
				.HasOne<ProductCategory>(s => s.ProductCategory)
				.WithMany(g => g.Products)
				.HasForeignKey(s => s.ProductCategoryId);
			modelBuilder.Entity<UserReceipt>()
				.HasOne<User>(s => s.User)
				.WithMany(g => g.UserReceipts)
				.HasForeignKey(s => s.UserId);
			modelBuilder.Entity<UserReceipt>()
				.HasOne<Receipt>(s => s.Receipt)
				.WithMany(g => g.UserReceipts)
				.HasForeignKey(s => s.ReceiptId);
			modelBuilder.CustomSeed();

		}
	}
	public static class CustomDbInitializer
	{
		public static void CustomSeed(this ModelBuilder modelBuilder)
		{
			RoleSeed(modelBuilder);
		}
		private static void RoleSeed(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Role>().HasData(
				new Role { Name = "Admin", Id = new Guid("68fec171-615c-4c79-84d8-d0672e8fa02a") },
				new Role { Name = "User", Id = new Guid("de0139a5-aba1-4194-85ce-ee16a0353598") }
				);
			modelBuilder.Entity<ProductCategory>().HasData(
				new ProductCategory { NameProductCategory = "Cold drinks", Id = new Guid("de0139a5-aba1-4194-85ce-ee16a0353598") },
				new ProductCategory { NameProductCategory = "Hot drinks", Id = new Guid("6e46b88b-f916-4ea7-a9c3-20a9f7ce90f7") },
				new ProductCategory { NameProductCategory = "Bakery", Id = new Guid("97c31953-ba41-4bfd-8605-12d98a7536e2") },
				new ProductCategory { NameProductCategory = "Cakes", Id = new Guid("5eeb5069-5fd5-41b9-96de-2bd15828c01f") },
				new ProductCategory { NameProductCategory = "Cookies", Id = new Guid("07dfb218-dfc3-4a90-bad8-571c4a4087fb") }
				);

		}
	}
}
