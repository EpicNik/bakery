﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Interfaces.Repositories
{
	public interface IGenericRepository<TEntity> where TEntity : class
	{
		IQueryable<TEntity> GetAll();
		Task<TEntity> GetById(Guid id);
		IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> expression);
		Task<TEntity> Add(TEntity entity);
		Task<TEntity> Update(TEntity entity);
		Task<TEntity> Delete(Guid id);
	}
}
