﻿using Bakery.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.Interfaces
{
	public interface IUnitOfWork:IDisposable
	{
		IAddressRepository  Addresses { get; }
		IBasketProductRepository BasketProducts { get; }
		IBasketRepository Baskets { get; }
		IProductCategoryRepository ProductCategories { get; }
		IProductRepository Products { get; }
		IReceiptProductRepository ReceiptProducts { get; }
		IReceiptRepository Receipts { get; }
		IReviewRepository Reviews { get; }
		IRoleRepository Roles { get; }
		IStockItemRepository StockItems { get; }
		IStockRepository Stocks { get; }
		IUserRepository Users { get; }

		IUserReceiptRepository UserReceipts { get; }
		ILogRepository Logs { get; }
	}
}
