﻿using Bakery.DAL.EF;
using Bakery.DAL.EF.Repositories;
using Bakery.DAL.Interfaces;
using Bakery.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.DAL.UnitOfWork
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly BakeryDBContext _context;
		public UnitOfWork(BakeryDBContext context)
		{
			this._context = context;
			Addresses = new AddressRepository(_context);
			BasketProducts = new BasketProductRepository(_context);
			Baskets = new BasketRepository(_context);
			ProductCategories = new ProductCategoryRepository(_context);
			Products = new ProductRepository(_context);
			ReceiptProducts = new ReceiptProductRepository(_context);
			Receipts = new ReceiptRepository(_context);
			Reviews = new ReviewRepository(_context);
			Roles = new RoleRepository(_context);
			StockItems = new StockItemRepository(_context);
			Stocks = new StockRepository(_context);
			Users = new UserRepository(_context);
			UserReceipts = new UserReceiptRepository(_context);
			Logs = new LogRepository(_context);
		}
		public IAddressRepository Addresses { get; private set; }


		public IBasketProductRepository BasketProducts { get; private set; }

		public IBasketRepository Baskets { get; private set; }


		public IProductCategoryRepository ProductCategories { get; private set; }

		public IProductRepository Products { get; private set; }

		public IReceiptProductRepository ReceiptProducts { get; private set; }

		public IReceiptRepository Receipts { get; private set; }

		public IReviewRepository Reviews { get; private set; }

		public IRoleRepository Roles { get; private set; }

		public IStockItemRepository StockItems { get; private set; }

		public IStockRepository Stocks { get; private set; }

		public IUserRepository Users { get; private set; }

		public IUserReceiptRepository UserReceipts { get; private set; }
		public ILogRepository Logs { get; private set; }

		public void Dispose()
		{
			_context.Dispose();
		}
	}
}
