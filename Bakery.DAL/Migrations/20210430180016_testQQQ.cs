﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bakery.DAL.Migrations
{
    public partial class testQQQ : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssortmenId",
                table: "ProductCategories");

            migrationBuilder.DropColumn(
                name: "DrinkId",
                table: "ProductCategories");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AssortmenId",
                table: "ProductCategories",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DrinkId",
                table: "ProductCategories",
                type: "uniqueidentifier",
                nullable: true);
        }
    }
}
