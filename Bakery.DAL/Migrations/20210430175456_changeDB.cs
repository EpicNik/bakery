﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bakery.DAL.Migrations
{
    public partial class changeDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductCategories_Assortmens_AssortmenId",
                table: "ProductCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductCategories_Drinks_DrinkId",
                table: "ProductCategories");

            migrationBuilder.DropTable(
                name: "Assortmens");

            migrationBuilder.DropTable(
                name: "Drinks");

            migrationBuilder.DropIndex(
                name: "IX_ProductCategories_AssortmenId",
                table: "ProductCategories");

            migrationBuilder.DropIndex(
                name: "IX_ProductCategories_DrinkId",
                table: "ProductCategories");

            migrationBuilder.CreateTable(
                name: "UserReceipt",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ReceiptId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserReceipt", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserReceipt_Receipts_ReceiptId",
                        column: x => x.ReceiptId,
                        principalTable: "Receipts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserReceipt_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "ProductCategories",
                columns: new[] { "Id", "AssortmenId", "DrinkId", "NameProductCategory" },
                values: new object[,]
                {
                    { new Guid("de0139a5-aba1-4194-85ce-ee16a0353598"), null, null, "Cold drinks" },
                    { new Guid("6e46b88b-f916-4ea7-a9c3-20a9f7ce90f7"), null, null, "Hot drinks" },
                    { new Guid("97c31953-ba41-4bfd-8605-12d98a7536e2"), null, null, "Bakery" },
                    { new Guid("5eeb5069-5fd5-41b9-96de-2bd15828c01f"), null, null, "Cakes" },
                    { new Guid("07dfb218-dfc3-4a90-bad8-571c4a4087fb"), null, null, "Cookies" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserReceipt_ReceiptId",
                table: "UserReceipt",
                column: "ReceiptId");

            migrationBuilder.CreateIndex(
                name: "IX_UserReceipt_UserId",
                table: "UserReceipt",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserReceipt");

            migrationBuilder.DeleteData(
                table: "ProductCategories",
                keyColumn: "Id",
                keyValue: new Guid("07dfb218-dfc3-4a90-bad8-571c4a4087fb"));

            migrationBuilder.DeleteData(
                table: "ProductCategories",
                keyColumn: "Id",
                keyValue: new Guid("5eeb5069-5fd5-41b9-96de-2bd15828c01f"));

            migrationBuilder.DeleteData(
                table: "ProductCategories",
                keyColumn: "Id",
                keyValue: new Guid("6e46b88b-f916-4ea7-a9c3-20a9f7ce90f7"));

            migrationBuilder.DeleteData(
                table: "ProductCategories",
                keyColumn: "Id",
                keyValue: new Guid("97c31953-ba41-4bfd-8605-12d98a7536e2"));

            migrationBuilder.DeleteData(
                table: "ProductCategories",
                keyColumn: "Id",
                keyValue: new Guid("de0139a5-aba1-4194-85ce-ee16a0353598"));

            migrationBuilder.CreateTable(
                name: "Assortmens",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NameAssortmen = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Assortmens", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Drinks",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NameDrink = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drinks", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategories_AssortmenId",
                table: "ProductCategories",
                column: "AssortmenId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductCategories_DrinkId",
                table: "ProductCategories",
                column: "DrinkId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductCategories_Assortmens_AssortmenId",
                table: "ProductCategories",
                column: "AssortmenId",
                principalTable: "Assortmens",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductCategories_Drinks_DrinkId",
                table: "ProductCategories",
                column: "DrinkId",
                principalTable: "Drinks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
