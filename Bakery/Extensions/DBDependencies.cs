﻿using Bakery.DAL.EF.Repositories;
using Bakery.DAL.Interfaces;
using Bakery.DAL.Interfaces.Repositories;
using Bakery.DAL.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Extensions
{
	public static class DBDependencies
	{
		public static void AddBakeryDependencies(this IServiceCollection services)
		{
			services.AddTransient<IAddressRepository, AddressRepository>();
			services.AddTransient<IBasketProductRepository, BasketProductRepository>();
			services.AddTransient<IBasketRepository, BasketRepository>();
			services.AddTransient<IProductCategoryRepository, ProductCategoryRepository>();
			services.AddTransient<IProductRepository, ProductRepository>();
			services.AddTransient<IReceiptProductRepository, ReceiptProductRepository>();
			services.AddTransient<IReceiptRepository, ReceiptRepository>();
			services.AddTransient<IReviewRepository, ReviewRepository>();
			services.AddTransient<IRoleRepository, RoleRepository>();
			services.AddTransient<IStockItemRepository, StockItemRepository>();
			services.AddTransient<IStockRepository, StockRepository>();
			services.AddTransient<IUserRepository, UserRepository>();
			services.AddTransient<IUnitOfWork, UnitOfWork>();
		}
	}
}
