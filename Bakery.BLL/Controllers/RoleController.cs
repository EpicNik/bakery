﻿using Bakery.DAL.Entities;
using Bakery.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.BLL.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RoleController: ControllerBase
	{
		private readonly IUnitOfWork _unitOfWork;
		public RoleController(IUnitOfWork unitOfWork)
		{
			this._unitOfWork = unitOfWork;
		}
		[HttpGet("GetAll")]
		public async Task<ActionResult<IEnumerable<Role>>> GetAllRoles()
		{
			var rolerList = await _unitOfWork.Roles.GetAll().ToListAsync();
			return Ok(rolerList);
		} 
		[HttpPost("Create")]
		public async Task<ActionResult<Role>> CreateRole(string name)
		{
			return Ok(await _unitOfWork.Roles.Add(new Role() { Name = name }));
		}
	}
}
