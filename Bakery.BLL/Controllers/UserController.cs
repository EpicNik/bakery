﻿using Bakery.DAL.Entities;
using Bakery.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.BLL.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController:ControllerBase
	{
		private readonly IUnitOfWork _unitOfWork;
		public UserController(IUnitOfWork unitOfWork)
		{
			this._unitOfWork = unitOfWork;
		}

		[HttpGet("GetAll")]
		public async Task<ActionResult<User>> GetAllUsers()
		{
			return Ok(await _unitOfWork.Users.GetAll().ToListAsync());
		}
		[HttpPost("Create")]
		public async Task<ActionResult<User>> CreateUser(User user)
		{
			return Ok(await _unitOfWork.Users.Add(user));
		}
	}
}
